﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10004163_Group10_Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<Dictionary<string, string>, Dictionary<string, int>> MarkDict = new Dictionary<Dictionary<string, string>, Dictionary<string, int>>();
            List<string> paperCodes = new List<string>();
            List<int> MarkList = new List<int>();
            List<string> letterMarks = new List<string>();
            MarkDict = setMarks();
            paperCodes = getPaperCodes(MarkDict);
            MarkList = getMarkInfo(MarkDict);
            letterMarks = getLetterMarks(MarkList);
            printMarks(MarkDict, paperCodes, MarkList, letterMarks);
            calculateAverage(MarkList);
            listA(MarkList, paperCodes);
            Console.ReadKey();
        }

        public static List<string> getLetterMarks(List<int> x)
        {
            List<string> Marks = new List<string>();

            foreach (int i in x)
            {
                string Mark;
                if (i >= 90)
                {
                    Mark = "A+";
                }
                else if (i <= 89 && i >= 85)
                {
                    Mark = "A";
                }
                else if (i <= 84 && i >= 80)
                {
                    Mark = "A-";
                }
                else if (i <= 79 && i >= 75)
                {
                    Mark = "B+";
                }
                else if (i <= 74 && i >= 70)
                {
                    Mark = "B";
                }
                else if (i <= 69 && i >= 65)
                {
                    Mark = "B-";
                }
                else if (i <= 64 && i >= 60)
                {
                    Mark = "C+";
                }
                else if (i <= 59 && i >= 55)
                {
                    Mark = "C";
                }
                else if (i <= 54 && i >= 50)
                {
                    Mark = "C-";
                }
                else if (i <= 49 && i >= 40)
                {
                    Mark = "D";
                }
                else
                {
                    Mark = "E";
                }

                Marks.Add(Mark);
            }

            return Marks;
        }
        public static List<string> getPaperCodes(Dictionary<Dictionary<string, string>, Dictionary<string, int>> x)
        {
            Dictionary<string, int> paperInfo = new Dictionary<string, int>();
            List<string> paperCodes = new List<string>();
            Dictionary<string, string> levelID = x.Keys.ElementAt(0);
            x.TryGetValue(levelID, out paperInfo);

            foreach (KeyValuePair<string, int> entry in paperInfo)
            {
                paperCodes.Add(entry.Key);
            }

            return paperCodes;
        }

        public static List<int> getMarkInfo(Dictionary<Dictionary<string, string>, Dictionary<string, int>> x)
        {
            Dictionary<string, int> paperInfo = new Dictionary<string, int>();
            List<int> MarkInfo = new List<int>();
            Dictionary<string, string> levelID = x.Keys.ElementAt(0);
            x.TryGetValue(levelID, out paperInfo);

            foreach (KeyValuePair<string, int> entry in paperInfo)
            {
                MarkInfo.Add(entry.Value);
            }

            return MarkInfo;
        }


        public static void calculateAverage(List<int> x)
        {
            double average = 0;
            int sum = 0;

            foreach (int i in x)
            {
                sum += i;
            }

            average = sum / x.Count();
            Console.WriteLine($"The average of the entered marks is: {average}");

            if (average <= 100 && average >= 50)
            {
                Console.WriteLine($"Overall the student has passed the year. ");
            }
            else
            {
                Console.WriteLine($"Overall the student has failed the year. ");
            }
        }

        public static void printMarks(Dictionary<Dictionary<string, string>, Dictionary<string, int>> MarkDict, List<string> paperCodes, List<int> MarkInfo, List<string> letterInfo)
        {
            Dictionary<string, string> levelID = new Dictionary<string, string>();
            string studentID;
            string level;
            levelID = MarkDict.Keys.ElementAt(0);
            studentID = levelID.Keys.ElementAt(0);
            level = levelID.Values.ElementAt(0);

            Console.WriteLine($"Student ID: {studentID}");
            Console.WriteLine($"Level of study: DAC{level}");
            Console.WriteLine($"Papers entered: {String.Join(",", paperCodes)}");
            Console.WriteLine($"Paper Percentages: {String.Join(",", MarkInfo)}");
            Console.WriteLine($"Paper Letter Grades: {String.Join(",", letterInfo)}");
        }


        public static void listA(List<int> MarkInfo, List<string> paperCodes)
        {
            List<string> highPapers = new List<string>();

            for (int i = 0; i < MarkInfo.Count(); i++)
            {
                if (MarkInfo.ElementAt(i) >= 90)
                {
                    highPapers.Add(paperCodes.ElementAt(i));
                }
            }

            if (highPapers.Count() > 0)
            {
                Console.WriteLine($"The student scored an 'A+' letter Grade in the following papers: {String.Join(", ", highPapers)}");
            }
            else
            {
                Console.WriteLine($"The student did not score an 'A+' in any subject.");
            }
        }

        public static Dictionary<Dictionary<string, string>, Dictionary<string, int>> setMarks()
        {
            int paperAmount = 0;
            bool validCheck = true;
            string level = "";
            Dictionary<string, string> levelID = new Dictionary<string, string>();
            Dictionary<string, int> paperInfo = new Dictionary<string, int>();
            Dictionary<Dictionary<string, string>, Dictionary<string, int>> MarkInfo = new Dictionary<Dictionary<string, string>, Dictionary<string, int>>();

            while (validCheck)
            {
                Console.WriteLine($"Please enter what level you are in using an integer: ");
                level = Console.ReadLine();

                if (level == "6")
                {
                    paperAmount = 3;
                    validCheck = false;
                }
                else if (level == "5")
                {
                    paperAmount = 4;
                    validCheck = false;
                }
                else {
                    Console.WriteLine($"Error: This is not a valid level please try again. ");
                }
            }

            for (int i = 0; i < paperAmount; i++)
            {
                Console.WriteLine($"Please enter the paper code for paper {i + 1}: ");
                string paperCode = Console.ReadLine();
                paperInfo.Add(paperCode, 0);
            }

            Console.WriteLine($"Please input your Student ID Number: ");
            string studentID = Console.ReadLine();

            for (int i = 0; i < paperAmount; i++)
            {
                validCheck = true;
                while (validCheck)
                {
                    Console.WriteLine($"Please enter one of the paper codes: ");
                    string paperCode = Console.ReadLine();
                    int paperMark = 0;

                    if (paperInfo.TryGetValue(paperCode, out paperMark))
                    {
                        paperMark = 0;
                        bool MarkCheck = true;

                        while (MarkCheck)
                        {
                            Console.WriteLine($"Please enter your mark for this paper rounded to the nearest %: ");
                            try
                            {
                                paperMark = int.Parse(Console.ReadLine());

                                if (paperMark <= 100 && paperMark >= 0)
                                {
                                    paperInfo.Remove(paperCode);
                                    paperInfo.Add(paperCode, paperMark);
                                    MarkCheck = false;
                                }
                                else {
                                    Console.WriteLine($"Error: Please enter an integer between 0 and 100.");
                                }
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine($"Error: Invalid Mark.");
                            }
                        }

                        validCheck = false;
                    }
                    else {
                        Console.WriteLine($"Error: Invalid paper code. ");
                    }
                }
            }
            levelID.Add(studentID, level);
            MarkInfo.Add(levelID, paperInfo);
            return MarkInfo;
        }
    }
}
